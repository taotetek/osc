package osc_test

import (
	"bytes"
	"encoding/binary"
	"testing"

	"github.com/poy/onpar"
	"github.com/poy/onpar/expect"
	"github.com/poy/onpar/matchers"
	"gitlab.com/taotetek/osc"
)

type expectation = expect.Expectation

var (
	chain        = matchers.Chain
	receive      = matchers.Receive
	haveOccurred = matchers.HaveOccurred
	not          = matchers.Not
	equal        = matchers.Equal
)

func TestClient(t *testing.T) {
	o := onpar.New()
	defer o.Run(t)

	o.BeforeEach(func(t *testing.T) (e expectation, c *osc.Client, writer1, writer2 *mockWriter) {
		wA := newMockWriter()
		wB := newMockWriter()
		expect := expect.New(t)
		c, err := osc.NewClient(osc.ToWriter(wA), osc.ToWriter(wB))
		expect(err).To(not(haveOccurred()))
		return expect, c, wA, wB
	})

	writeLen := func(input, output chan []byte, length chan int) {
		for b := range input {
			length <- len(b)
			output <- b
		}
	}

	o.Group("when all writers are returning expected values", func() {
		o.BeforeEach(func(expect expectation, client *osc.Client, writer1, writer2 *mockWriter) (e expectation, c *osc.Client, written1, written2 chan []byte) {
			written1 = make(chan []byte, 100)
			go writeLen(writer1.WriteInput.B, written1, writer1.WriteOutput.N)
			close(writer1.WriteOutput.Err)

			written2 = make(chan []byte, 100)
			go writeLen(writer2.WriteInput.B, written2, writer2.WriteOutput.N)
			close(writer2.WriteOutput.Err)

			return expect, client, written1, written2
		})

		o.Spec("it writes ControlVoltageA to all writers", func(expect expectation, c *osc.Client, written1, written2 chan []byte) {
			c.SetControlVoltageA(1)

			expected := bytes.NewBuffer(padded([]byte("/osm/a/cv")))
			expected.Write(padded([]byte(",f")))
			binary.Write(expected, binary.BigEndian, float32(1))
			expect(written1).To(chain(receive(), equal(expected.Bytes())))
			expect(written2).To(chain(receive(), equal(expected.Bytes())))
		})

		o.Spec("it writes ControlVoltageB to all writers", func(expect expectation, c *osc.Client, written1, written2 chan []byte) {
			c.SetControlVoltageB(1)

			expected := bytes.NewBuffer(padded([]byte("/osm/b/cv")))
			expected.Write(padded([]byte(",f")))
			binary.Write(expected, binary.BigEndian, float32(1))
			expect(written1).To(chain(receive(), equal(expected.Bytes())))
			expect(written2).To(chain(receive(), equal(expected.Bytes())))
		})

		const triggerIntPath = "/osm/a/tr,i"

		o.Spec("it writes TriggerA to all writers", func(expect expectation, c *osc.Client, written1, written2 chan []byte) {
			c.SetTriggerA(1)

			expected := bytes.NewBuffer(padded([]byte("/osm/a/tr")))
			expected.Write(padded([]byte(",i")))
			binary.Write(expected, binary.BigEndian, int32(1))
			expect(written1).To(chain(receive(), equal(expected.Bytes())))
			expect(written2).To(chain(receive(), equal(expected.Bytes())))
		})

		o.Spec("it writes TriggerB to all writers", func(expect expectation, c *osc.Client, written1, written2 chan []byte) {
			c.SetTriggerB(1)

			expected := bytes.NewBuffer(padded([]byte("/osm/b/tr")))
			expected.Write(padded([]byte(",i")))
			binary.Write(expected, binary.BigEndian, int32(1))
			expect(written1).To(chain(receive(), equal(expected.Bytes())))
			expect(written2).To(chain(receive(), equal(expected.Bytes())))
		})
	})
}

func padded(b []byte) []byte {
	const alignTo = 4
	remain := len(b) % alignTo
	needed := alignTo - remain
	return append(b, make([]byte, needed)...)
}

type mockWriter struct {
	WriteInput struct {
		B chan []byte
	}
	WriteOutput struct {
		N   chan int
		Err chan error
	}
}

func newMockWriter() *mockWriter {
	var m mockWriter
	m.WriteInput.B = make(chan []byte, 100)
	m.WriteOutput.N = make(chan int, 100)
	m.WriteOutput.Err = make(chan error, 100)
	return &m
}

func (w *mockWriter) Write(b []byte) (n int, err error) {
	w.WriteInput.B <- b
	return <-w.WriteOutput.N, <-w.WriteOutput.Err
}
