package osc

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"net"
)

type oscControlVoltageAddress string
type oscTriggerAddress string

const (
	triggerA oscTriggerAddress = "/osm/a/tr"
	triggerB oscTriggerAddress = "/osm/b/tr"

	controlVoltageA oscControlVoltageAddress = "/osm/a/cv"
	controlVoltageB oscControlVoltageAddress = "/osm/b/cv"

	typeTagInt32   = "i"
	typeTagFloat32 = "f"
	typeTagString  = "s"
	typeTagBlob    = "b"

	typeTagStart = ","
)

// Opt is an option which can be passed to NewClient.
type Opt func(Client) (Client, error)

// ToNet returns an Opt that will write messages to a
// network address.
func ToNet(network, address string) Opt {
	return func(c Client) (Client, error) {
		conn, err := net.Dial(network, address)
		if err != nil {
			return c, err
		}
		return ToWriter(conn)(c)
	}
}

// ToWriter returns an Opt that will write messages to an
// io.Writer
func ToWriter(w io.Writer) Opt {
	return func(c Client) (Client, error) {
		c.writers = append(c.writers, w)
		return c, nil
	}
}

func writePaddedString(buf *bytes.Buffer, data string) error {
	n, err := buf.WriteString(data)
	if err != nil {
		return err
	}
	numPadBytes := 4*(n/4+1) - n
	if numPadBytes > 0 {
		pad := make([]byte, numPadBytes)
		_, err := buf.Write(pad)
		if err != nil {
			return err
		}
	}
	return nil
}

type Client struct {
	writers []io.Writer
}

func NewClient(required Opt, extra ...Opt) (*Client, error) {
	var (
		c   Client
		err error
	)
	for _, o := range append([]Opt{required}, extra...) {
		c, err = o(c)
		if err != nil {
			return nil, err
		}
	}
	return &c, nil
}

func (c *Client) setControlVoltage(addr oscControlVoltageAddress, data float32) error {
	var buf bytes.Buffer
	err := writePaddedString(&buf, string(addr))
	if err != nil {
		return err
	}

	err = writePaddedString(&buf, fmt.Sprintf("%s%s", typeTagStart, typeTagFloat32))
	if err != nil {
		return err
	}

	err = binary.Write(&buf, binary.BigEndian, data)
	if err != nil {
		return err
	}

	for _, w := range c.writers {
		_, err := w.Write(buf.Bytes())
		if err != nil {
			return err
		}
	}
	return nil
}

func (c *Client) SetControlVoltageA(data float32) error {
	err := c.setControlVoltage(controlVoltageA, data)
	return err
}

func (c *Client) SetControlVoltageB(data float32) error {
	err := c.setControlVoltage(controlVoltageB, data)
	return err
}

func (c *Client) setTrigger(addr oscTriggerAddress, data int32) error {
	var buf bytes.Buffer
	err := writePaddedString(&buf, string(addr))
	if err != nil {
		return err
	}

	err = writePaddedString(&buf, fmt.Sprintf("%s%s", typeTagStart, typeTagInt32))
	if err != nil {
		return err
	}

	err = binary.Write(&buf, binary.BigEndian, data)
	if err != nil {
		return err
	}

	for _, w := range c.writers {
		_, err = w.Write(buf.Bytes())
		if err != nil {
			return err
		}
	}
	return nil
}

func (c *Client) SetTriggerA(data int32) error {
	err := c.setTrigger(triggerA, data)
	return err
}

func (c *Client) SetTriggerB(data int32) error {
	err := c.setTrigger(triggerB, data)
	return err
}

func (c *Client) Close() {
	for _, w := range c.writers {
		closer, ok := w.(io.Closer)
		if !ok {
			continue
		}
		closer.Close()
	}
}
